import type { NextPage } from "next";
import { useEffect, useRef, useState } from "react";
import Head from "next/head";
import Image from "next/image";
import PhoneIcon from "@mui/icons-material/Phone";
import {
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Typography,
} from "@mui/material";
import Link from "next/link";
import { isLoggedIn } from "Helpers/auth-helper";
import { useDispatch } from "react-redux";
import { onError, onErrorFinish } from "Redux/slice/errorSlice";
import { onLoading, onLoadingFinish } from "Redux/slice/loadingSlice";
import { getAuthToken } from "Helpers/auth-helper";
import { onSuccess, onSuccessFinish } from "Redux/slice/successSlice";

const Home: NextPage = () => {
  const [imagesData, setImagesData] = useState([]);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [toDeleteCategory, setToDeleteCategory] = useState({
    name: "",
    _id: "",
  });
  const loggedIn = useRef(false);
  const dispatch = useDispatch();

  useEffect(() => {
    async function fetchImagesData() {
      dispatch(onLoading("RETRIEVE_CATEGORY_DATA"));
      let response = await fetch("/api/category/retrieve");
      let result = await response.json();
      dispatch(onLoadingFinish("RETRIEVE_CATEGORY_DATA"));

      if (result.success) {
        setImagesData(result.data);
      } else {
        dispatch(onError(result.message));
        setTimeout(() => dispatch(onErrorFinish(result.message)), 6000);
      }
    }
    loggedIn.current = isLoggedIn();
    fetchImagesData();
  }, [dispatch]);

  const deleteCategory = async (categoryId: string) => {
    dispatch(onLoading("DELETE_CATEGORY_DATA"));
    let response = await fetch("/api/category/delete", {
      method: "POST",
      headers: {
        Authorization: getAuthToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        category_id: categoryId,
      }),
    });
    dispatch(onLoadingFinish("DELETE_CATEGORY_DATA"));
    let result = await response.json();

    if (result.success) {
      dispatch(onSuccess(result.message));
      setTimeout(() => dispatch(onSuccessFinish(result.message)), 6000);
      setImagesData((imagesData) => {
        return imagesData.filter(
          (imageData: any) => imageData._id !== toDeleteCategory._id
        );
      });
      setToDeleteCategory({ name: "", _id: "" });
    } else {
      dispatch(onError(result.message));
      setTimeout(() => dispatch(onErrorFinish(result.message)), 6000);
    }
  };
  return (
    <div className="bg-gray-100">
      <Head>
        <title>One DC Moto</title>
        <meta name="description" content="One DC Moto" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        {loggedIn.current ? (
          <Link href="/category/insert" passHref>
            <div>
              <Button
                variant="contained"
                className="text-black shadow-none z-50 bg-transparent w-full"
              >
                Insert new Category
              </Button>
            </div>
          </Link>
        ) : (
          <></>
        )}
        <Grid container direction="column">
          <Grid item container justifyContent="center" className="bg-black">
            <Grid
              item
              container
              direction="column"
              md={5}
              lg={4}
              xl={2}
              className="bg-gray-800 mt-2 drop-shadow-2xl"
            >
              <Image
                src="/item-assets/other/Impression-DC.jpg"
                alt="Black-white logo"
                width="100%"
                height="400px"
              />
            </Grid>
          </Grid>
          <Grid item container className="bg-black w-full pb-4">
            <Grid
              item
              container
              justifyContent="center"
              xs={12}
              alignItems="center"
            >
              <PhoneIcon className="text-[#9b9d76]" />
              <Typography
                className="text-[#9b9d76]"
                variant="body2"
                color="initial"
              >
                +62817801718
              </Typography>
            </Grid>
          </Grid>
          <Grid item container justifyContent="center">
            <Grid item container xs={11}>
              {imagesData.map((imageData: any) => {
                return (
                  <Grid
                    item
                    container
                    key={`${imageData.name} - ${imageData.imagepath}`}
                  >
                    {/* <Grid item container xs={12} justifyContent="center">
                      <Typography
                        variant="h6"
                        color="initial"
                        className="text-gray-400 font-bold italic"
                      >
                        {imageData.name}
                      </Typography>
                    </Grid> */}
                    <Grid
                      key={imageData._id}
                      item
                      xs={12}
                      className="m-5 shadow-black shadow-xl cursor-pointer border-black"
                    >
                      <Link href={`/category/${imageData.name}`} passHref>
                        <div>
                          <Image
                            src={imageData.imagepath}
                            alt={imageData.name}
                            height={100}
                            width="100%"
                            layout="responsive"
                          />
                        </div>
                      </Link>
                      {loggedIn.current ? (
                        <Grid item container className="border-slate-900">
                          <Grid item xs={6}>
                            <Link
                              href={`/category/${imageData.name}/edit`}
                              passHref
                            >
                              <Grid container justifyContent="center">
                                <Grid item>
                                  <Button
                                    variant="text"
                                    className="text-black"
                                    fullWidth
                                  >
                                    Edit
                                  </Button>
                                </Grid>
                              </Grid>
                            </Link>
                          </Grid>
                          <Grid
                            item
                            container
                            xs={6}
                            justifyContent="center"
                            className=" bg-red-800"
                          >
                            <Button
                              variant="text"
                              className="text-white"
                              fullWidth
                              onClick={(e) => {
                                e.stopPropagation();
                                setToDeleteCategory(imageData);
                                setOpenDeleteDialog(true);
                              }}
                            >
                              delete
                            </Button>
                          </Grid>
                        </Grid>
                      ) : (
                        <></>
                      )}
                    </Grid>
                  </Grid>
                );
              })}
            </Grid>
          </Grid>
        </Grid>
        <Dialog
          open={openDeleteDialog}
          onClose={() => setOpenDeleteDialog(false)}
        >
          <DialogTitle>Confirm Delete Category</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Are you sure to delete{" "}
              <span className="font-bold">{toDeleteCategory.name}</span>?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => setOpenDeleteDialog(false)}
              variant="contained"
              className="shadow-2xl text-red-700 bg-white hover:bg-gray-100"
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                deleteCategory(toDeleteCategory._id);
                setOpenDeleteDialog(false);
              }}
              variant="contained"
              className="text-white bg-red-700"
            >
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </main>
    </div>
  );
};

export default Home;
