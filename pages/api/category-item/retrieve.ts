import type { NextApiRequest } from "next";
import dbConnect from "../dbConnect";
import { checkToken } from "ApiHelpers/check-auth-helper";
import Joi from "joi";
import { retrieveCategoryItemSchema } from "ApiHelpers/validator-schema";
import CategoryItem from "Models/CategoryItem";
import Category from "Models/Category";

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      try {
        let categoryItem: any;
        const inputs: any = req.query;

        const validateInput = retrieveCategoryItemSchema.validate(inputs);

        if (validateInput.error) {
          res.status(422).json({
            success: false,
            message: validateInput.error.details[0].message,
          });
          return res;
        }
        const per_page = inputs.per_page || 100;

        if (inputs.category_name) {
          const category = await Category.findOne({
            name: inputs.category_name,
          });
          if (category) {
            inputs.category_id = category._id;
          } else {
            res.status(200).json({ success: true, data: [] });
            return res;
          }
        }

        if (inputs.id) {
          categoryItem = await CategoryItem.findById(inputs.id).lean();
        } else {
          categoryItem = await CategoryItem.find(inputs)
            .sort({ sequence: 1 })
            .limit(per_page)
            .lean();
        }

        res.status(200).json({ success: true, data: categoryItem });
        return res;
      } catch (err) {
        res.status(500).json({ success: false, message: "Unknown error" });
        return res;
      }
      break;
    case "POST":
      res.status(405).json({ success: false, message: "Method not allowed!" });
      break;
    default:
      res.status(400).json({ success: false, message: "Unauthorized!" });
      break;
  }
  return res;
}
