import type { NextApiRequest } from "next";
import dbConnect from "../dbConnect";
import { checkToken } from "ApiHelpers/check-auth-helper";
import CategoryItem from "Models/CategoryItem";
import Category from "Models/Category";
import mongoose from "mongoose";
import Joi from "joi";
import { insertCategoryItemSchema } from "ApiHelpers/validator-schema";

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      res.status(405).json({ success: false, message: "Method not allowed!" });
      break;
    case "POST":
      const verified = await checkToken(req.headers.authorization);

      if (!verified) {
        res.status(401).json({ success: false, message: "Unauthorized" });
        return res;
      }

      const inputs = req.body;
      const validateInput = insertCategoryItemSchema.validate(inputs);

      if (validateInput.error) {
        res.status(422).json({
          success: false,
          message: validateInput.error.details[0].message,
        });
        return res;
      }

      const categoryItem = await CategoryItem.findOne({ name: inputs.name });

      if (categoryItem) {
        res
          .status(422)
          .json({ success: false, message: "name is already registered!" });
        return res;
      }

      const newCategoryItem = new CategoryItem(inputs);
      const category: any = await Category.findOne({
        name: inputs.category_name,
      });

      if (!category) {
        res
          .status(404)
          .json({ success: false, message: "category_name not found!" });
        return res;
      }

      try {
        const sess = await mongoose.startSession();
        sess.startTransaction();
        category.category_items.push(newCategoryItem);
        newCategoryItem.category_id = category._id;
        await category.save({ session: sess });
        await newCategoryItem.save({ session: sess });
        await sess.commitTransaction();

        res
          .status(200)
          .json({ success: true, message: "Success input new category item!" });
      } catch (err) {
        console.log(err);
        res.status(200).json({
          success: 500,
          message: "Unknown error while inputting new category!",
        });
      }

      break;
    default:
      res.status(401).json({ success: false, message: "Unauthorized!" });
      break;
  }
  return res;
}
