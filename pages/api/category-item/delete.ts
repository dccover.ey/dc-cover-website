import type { NextApiRequest } from "next";
import dbConnect from "../dbConnect";
import { checkToken } from "ApiHelpers/check-auth-helper";
import Category from "Models/Category";
import { deleteCategoryItemSchema } from "ApiHelpers/validator-schema";
import mongoose from "mongoose";
import CategoryItem from "Models/CategoryItem";

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      res.status(405).json({ success: false, message: "Method not allowed!" });
      break;
    case "POST":
      const verified = await checkToken(req.headers.authorization);

      if (!verified) {
        res.status(401).json({ success: false, message: "Unauthorized" });
        return res;
      }

      const inputs = req.body;
      const validateInput = deleteCategoryItemSchema.validate(inputs);

      if (validateInput.error) {
        res.status(422).json({
          success: false,
          message: validateInput.error.details[0].message,
        });
        return res;
      }

      const categoryItem = await CategoryItem.findById(
        inputs.category_item_id
      ).populate("category_id");

      if (!categoryItem || !categoryItem.category_id) {
        res
          .status(404)
          .json({ success: false, message: "Category item not found!" });
        return res;
      }

      try {
        const sess = await mongoose.startSession();
        sess.startTransaction();
        categoryItem.category_id.category_items.pull(categoryItem);
        await categoryItem.remove({ session: sess });
        await categoryItem.category_id.save({ session: sess });
        await sess.commitTransaction();

        res.status(200).json({
          success: true,
          message: `Success delete ${categoryItem.name} item!`,
        });
      } catch (err) {
        console.log(err);
        res.status(200).json({
          success: 500,
          message: "Unknown error while deleting the item!",
        });
      }

      break;
    default:
      res.status(400).json({ success: false, message: "Unauthorized!" });
      break;
  }
  return res;
}
