import type { NextApiRequest } from "next";
import dbConnect from "../dbConnect";
import { checkToken } from "ApiHelpers/check-auth-helper";
import CategoryItem from "Models/CategoryItem";
import { editCategoryItemSchema } from "ApiHelpers/validator-schema";
import Joi from "joi";

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      res.status(405).json({ success: false, message: "Method not allowed!" });
      break;
    case "POST":
      const verified = await checkToken(req.headers.authorization);
      if (!verified) {
        res.status(401).json({ success: false, message: "Unauthorized" });
        return res;
      }

      const inputs = req.body;
      const validateInput = editCategoryItemSchema.validate(inputs);

      if (validateInput.error) {
        res.status(422).json({
          success: false,
          message: validateInput.error.details[0].message,
        });
        return res;
      }

      if (inputs.old_name !== inputs.name) {
        const categoryItem = await CategoryItem.findOne({
          name: inputs.name,
          category_id: inputs.category_id,
        });

        if (categoryItem) {
          res
            .status(400)
            .json({ success: false, message: "name is already registered!" });
          return res;
        }
      }

      const newCategoryItem = await CategoryItem.findOne({
        name: inputs.old_name,
      });

      try {
        newCategoryItem.name = inputs.name;
        newCategoryItem.description = inputs.description;
        newCategoryItem.sequence = inputs.sequence;
        newCategoryItem.imagepath = inputs.imagepath;
        await newCategoryItem.save();

        res.status(200).json({ success: true, message: "Success edit!" });
      } catch (err) {
        console.log(err);
        res.status(200).json({
          success: 500,
          message: "Unknown error while inputting new category!",
        });
      }

      break;
    default:
      res.status(400).json({ success: false, message: "Unauthorized!" });
      break;
  }
  return res;
}
