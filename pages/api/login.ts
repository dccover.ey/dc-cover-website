import type { NextApiRequest } from "next";
import dbConnect from "./dbConnect";
import UserModel from "models/User";

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      return res.status(405).json({ success: false });
      break;
    case "POST":
      let user: any;
      const inputs = req.body;
      try {
        user = await UserModel.findOne({ email: inputs.email });
      } catch (err) {
        console.log(err);
        return res
          .status(400)
          .json({ success: false, message: "Unauthorized!" });
      }

      if (!user) {
        return res
          .status(401)
          .json({ success: false, message: "Unauthorized!" });
      }

      await bcrypt
        .compare(inputs.password, user.password)
        .then(function (result: boolean) {
          if (!result) {
            return res
              .status(401)
              .json({ success: false, message: "Unauthorized!" });
          } else {
            // s3 delete password from data and send data and token
            const jwtParam = {
              email: user.email,
              role: user.role,
            };
            const token = jwt.sign(
              { data: jwtParam },
              process.env.JWT_AUTH_SECRET,
              {
                expiresIn: 60 * 60 * 24 * 3, // 3 days
              }
            );

            return res.status(200).json({
              ...jwtParam,
              token,
              success: true,
              message: "Success Logged in!",
            });
          }
        });

      break;
    default:
      return res.status(400).json({ success: false, message: "Unauthorized!" });
      break;
  }
}
