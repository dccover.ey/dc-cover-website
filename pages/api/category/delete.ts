import type { NextApiRequest } from "next";
import dbConnect from "../dbConnect";
import { checkToken } from "ApiHelpers/check-auth-helper";
import Category from "Models/Category";
import { deleteCategorySchema } from "ApiHelpers/validator-schema";
import mongoose from "mongoose";
import CategoryItem from "Models/CategoryItem";

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      res.status(405).json({ success: false, message: "Method not allowed!" });
      break;
    case "POST":
      const verified = await checkToken(req.headers.authorization);

      if (!verified) {
        res.status(401).json({ success: false, message: "Unauthorized" });
        return res;
      }

      const inputs = req.body;
      const validateInput = deleteCategorySchema.validate(inputs);

      if (validateInput.error) {
        res.status(422).json({
          success: false,
          message: validateInput.error.details[0].message,
        });
        return res;
      }

      const category = await Category.findById(inputs.category_id);

      if (!category) {
        res
          .status(404)
          .json({ success: false, message: "Category not found!" });
        return res;
      }

      try {
        const sess = await mongoose.startSession();
        sess.startTransaction();
        await category.remove({ session: sess });
        await CategoryItem.deleteMany({
          _id: category.category_items,
        }).session(sess);
        await sess.commitTransaction();

        res.status(200).json({
          success: true,
          message: `Success delete ${category.name} category!`,
        });
      } catch (err) {
        console.log(err);
        res.status(200).json({
          success: 500,
          message: "Unknown error while inputting new category!",
        });
      }

      break;
    default:
      res.status(400).json({ success: false, message: "Unauthorized!" });
      break;
  }
  return res;
}
