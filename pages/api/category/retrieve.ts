import type { NextApiRequest } from "next";
import dbConnect from "../dbConnect";
import { checkToken } from "ApiHelpers/check-auth-helper";
import Joi from "joi";
import { retrieveCategorySchema } from "ApiHelpers/validator-schema";
import Category from "Models/Category";

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      try {
        let category: any;

        const inputs: any = req.query;

        const validateInput = retrieveCategorySchema.validate(inputs);

        if (validateInput.error) {
          res.status(422).json({
            success: false,
            message: validateInput.error.details[0].message,
          });
          return res;
        }
        const per_page = inputs.per_page || 100;

        if (inputs.id) {
          category = await Category.findById(inputs.id).lean();
        } else {
          category = await Category.find(inputs)
            .sort({ sequence: 1 })
            .limit(per_page)
            .lean();
        }

        res.status(200).json({ success: true, data: category });
        return res;
      } catch (err) {
        res.status(500).json({ success: false, message: "Unknown error" });
        return res;
      }
      break;
    case "POST":
      res.status(405).json({ success: false, message: "Method not allowed!" });
      break;
    default:
      res.status(400).json({ success: false, message: "Unauthorized!" });
      break;
  }
  return res;
}
