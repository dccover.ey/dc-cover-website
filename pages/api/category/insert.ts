import type { NextApiRequest } from "next";
import dbConnect from "../dbConnect";
import { checkToken } from "ApiHelpers/check-auth-helper";
import Category from "Models/Category";
import { insertCategorySchema } from "ApiHelpers/validator-schema";
import Joi from "joi";

export default async function handler(req: NextApiRequest, res: any) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      res.status(405).json({ success: false, message: "Method not allowed!" });
      break;
    case "POST":
      const verified = await checkToken(req.headers.authorization);

      if (!verified) {
        res.status(401).json({ success: false, message: "Unauthorized" });
        return res;
      }

      const inputs = req.body;
      const validateInput = insertCategorySchema.validate(inputs);

      if (validateInput.error) {
        res.status(422).json({
          success: false,
          message: validateInput.error.details[0].message,
        });
        return res;
      }

      const category = await Category.findOne({ name: inputs.name });

      if (category) {
        res
          .status(400)
          .json({ success: false, message: "name is already registered!" });
        return res;
      }

      const newCategory = new Category(inputs);

      try {
        await newCategory.save();

        res
          .status(200)
          .json({ success: true, message: "Success input new category!" });
      } catch (err) {
        console.log(err);
        res.status(200).json({
          success: 500,
          message: "Unknown error while inputting new category!",
        });
      }

      break;
    default:
      res.status(400).json({ success: false, message: "Unauthorized!" });
      break;
  }
  return res;
}
