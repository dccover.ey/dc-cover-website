import type { NextApiRequest } from "next";
import { checkToken } from "ApiHelpers/check-auth-helper";
const jwt = require("jsonwebtoken");

export default async function handler(req: NextApiRequest, res: any) {
  const { headers } = req;

  const verified = await checkToken(headers.authorization);

  if (verified) {
    res.status(200).json({ success: true });
  } else {
    res.status(401).json({
      success: false,
      message: "Session Expired, Please login again!",
    });
  }
}
