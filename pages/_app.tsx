import "../styles/globals.css";
import type { AppProps } from "next/app";
import CustomFooter from "Components/CustomFooter";
import CustomAppBar from "Components/Appbar";
import { Provider } from "react-redux";
import store from "Redux/store/store";
import Spinner from "Components/Spinner";
import ErrorBar from "Components/Snackbar/ErrorBar";
import SuccessBar from "Components/Snackbar/SuccessBar";
import CheckAuth from "Components/CheckAuth";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <CheckAuth />
      <Spinner />
      <CustomAppBar />
      <Component {...pageProps} />
      <CustomFooter />
      <ErrorBar />
      <SuccessBar />
    </Provider>
  );
}

export default MyApp;
