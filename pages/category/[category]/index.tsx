import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import {
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Typography,
} from "@mui/material";
import Link from "next/link";
import { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import { getAuthToken, isLoggedIn } from "Helpers/auth-helper";
import { useDispatch } from "react-redux";
import { onError, onErrorFinish } from "Redux/slice/errorSlice";
import { onLoading, onLoadingFinish } from "Redux/slice/loadingSlice";
import { onSuccess, onSuccessFinish } from "Redux/slice/successSlice";

const Category: NextPage = () => {
  const [imagesData, setImagesData] = useState([]);
  const [openImageDialog, setOpenImageDialog] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const [selectedName, setSelectedName] = useState("");
  const [toDeleteItem, setToDeleteItem] = useState({ name: "", _id: "" });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);

  const router = useRouter();
  const dispatch = useDispatch();
  const loggedIn = useRef(false);

  const onImageClick = (imageUrl: string, imageName: string) => {
    setOpenImageDialog(true);
    setSelectedName(imageName);
    setSelectedImage(imageUrl);
  };

  useEffect(() => {
    if (router.asPath === router.pathname) {
      return;
    }

    const newAsPath = router.query.category;

    async function fetchImagesData() {
      dispatch(onLoading("FETCH_CATEGORY_ITEM"));
      let response = await fetch(
        `/api/category-item/retrieve?category_name=${newAsPath}`
      );
      let result = await response.json();
      dispatch(onLoadingFinish("FETCH_CATEGORY_ITEM"));
      if (result.success) {
        setImagesData(result.data);
      } else {
        dispatch(onError(result.message));
        setTimeout(() => dispatch(onErrorFinish(result.message)), 6000);
      }
    }

    loggedIn.current = isLoggedIn();
    fetchImagesData();
  }, [router.asPath, router.pathname, dispatch, router.query.category]);

  const deleteItem = async (itemId: string) => {
    dispatch(onLoading("DELETE_CATEGORY_ITEM_DATA"));
    let response = await fetch("/api/category-item/delete", {
      method: "POST",
      headers: {
        Authorization: getAuthToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        category_item_id: itemId,
      }),
    });
    dispatch(onLoadingFinish("DELETE_CATEGORY_ITEM_DATA"));
    let result = await response.json();

    if (result.success) {
      dispatch(onSuccess(result.message));
      setTimeout(() => dispatch(onSuccessFinish(result.message)), 6000);
      setImagesData((imagesData) => {
        return imagesData.filter(
          (imageData: any) => imageData._id !== toDeleteItem._id
        );
      });
      setToDeleteItem({ name: "", _id: "" });
    } else {
      dispatch(onError(result.message));
      setTimeout(() => dispatch(onErrorFinish(result.message)), 6000);
    }
  };

  return router.asPath !== router.pathname ? (
    <>
      <div>
        <Head>
          <title>DC Cover {router.asPath}</title>
          <meta name="description" content={`DC Cover ${router.asPath}`} />
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          {isLoggedIn() ? (
            <Link href={`/category/${router.query.category}/insert`} passHref>
              <div>
                <Button
                  variant="contained"
                  className="text-black shadow-none z-50 bg-transparent w-full"
                >
                  Insert new Category
                </Button>
              </div>
            </Link>
          ) : (
            <></>
          )}
          <Grid item container justifyContent="center">
            <Grid
              item
              container
              xs={12}
              justifyContent="center"
              className="bg-black/80 mb-5 py-3"
            >
              <Typography variant="h5" className="text-white italic ">
                Category : {router.query.category}
              </Typography>
            </Grid>
            <Grid item container xs={11}>
              {imagesData.map((imageData: any) => {
                return (
                  <>
                    <Grid item container>
                      {/* <Grid item container xs={12} justifyContent="center">
                        <Typography
                          variant="h6"
                          color="initial"
                          className="font-bold"
                        >
                          {imageData.name}
                        </Typography>
                      </Grid> */}
                      <Grid
                        item
                        xs={12}
                        className="m-5 shadow-lg shadow-black"
                        key={imageData._id}
                        onClick={() => {
                          onImageClick(imageData.imagepath, imageData.name);
                        }}
                      >
                        <div>
                          <Image
                            src={imageData.imagepath}
                            alt={imageData.name}
                            height={100}
                            width="100%"
                            layout="responsive"
                          />
                        </div>
                        {loggedIn.current ? (
                          <Grid item container className="border-slate-900">
                            <Grid item container xs={6} justifyContent="center">
                              <Link
                                href={`/category/${router.query.category}/${imageData.name}/edit`}
                                passHref
                              >
                                <Grid container justifyContent="center">
                                  <Grid item>
                                    <Button
                                      variant="text"
                                      className="text-black"
                                      fullWidth
                                    >
                                      Edit
                                    </Button>
                                  </Grid>
                                </Grid>
                              </Link>
                            </Grid>
                            <Grid
                              item
                              container
                              xs={6}
                              justifyContent="center"
                              className="bg-red-800"
                            >
                              <Button
                                className="text-white"
                                variant="text"
                                onClick={(e) => {
                                  e.stopPropagation();
                                  setOpenDeleteDialog(true);
                                  setToDeleteItem(imageData);
                                }}
                                fullWidth
                              >
                                delete
                              </Button>
                            </Grid>
                          </Grid>
                        ) : (
                          <></>
                        )}
                      </Grid>
                    </Grid>
                  </>
                );
              })}
            </Grid>
          </Grid>
        </main>
      </div>
      <Dialog
        open={openImageDialog}
        onClose={() => setOpenImageDialog(false)}
        fullScreen
      >
        <DialogContent className="bg-gray-800">
          <Typography variant="h6" className="text-white my-5">
            {selectedName}
          </Typography>
          <div className="drop-shadow-[4px_4px_15px_#FFFFFF]">
            <Image
              src={selectedImage}
              alt={"Selected Image"}
              height="100%"
              width="100%"
              layout="responsive"
            />
          </div>
        </DialogContent>
        <DialogActions className="bg-gray-800 ">
          <Button
            className="text-white"
            onClick={() => setOpenImageDialog(false)}
          >
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
      >
        <DialogTitle>Confirm Delete Category</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure to delete{" "}
            <span className="font-bold">{toDeleteItem.name}</span>?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => setOpenDeleteDialog(false)}
            variant="contained"
            className="shadow-2xl text-red-700 bg-white hover:bg-gray-100"
          >
            Cancel
          </Button>
          <Button
            onClick={() => {
              deleteItem(toDeleteItem._id);
              setOpenDeleteDialog(false);
            }}
            variant="contained"
            className="text-white bg-red-700"
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  ) : (
    <div></div>
  );
};

export default Category;
