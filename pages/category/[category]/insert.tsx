import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { Grid, Typography, TextField, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { onLoadingFinish, onLoading } from "Redux/slice/loadingSlice";
import { onError, onErrorFinish } from "Redux/slice/errorSlice";
import { onSuccess, onSuccessFinish } from "Redux/slice/successSlice";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { getAuthToken } from "Helpers/auth-helper";

const Category: NextPage = () => {
  const [name, setName] = useState("");
  const [imageApplied, setImageApplied] = useState(false);
  const [imagepath, setImagepath] = useState("");
  const [description, setDescription] = useState("");
  const [imageAlt, setImageAlt] = useState("");
  const [sequence, setSequence] = useState("");
  const [imageError, setImageError] = useState([]);

  const router = useRouter();
  const dispatch = useDispatch();
  const checkImagepath = (imagepath: string) => {
    setImageError([]);
    let validImage = false;
    let finalImagepath = imagepath;

    const allowedUrls = process.env.NEXT_PUBLIC_IMAGE_DOMAIN;

    if (!allowedUrls) {
      dispatch(onError("IMAGE URL CONFIG IS NOT SET!"));
      setTimeout(
        () => dispatch(onErrorFinish("IMAGE URL CONFIG IS NOT SET!")),
        6000
      );
      return;
    }
    for (let i = 0; i < allowedUrls.length; i++) {
      if (finalImagepath.includes(allowedUrls[i])) {
        validImage = true;
      }
    }

    if (!validImage) {
      dispatch(onError("IMAGE URL IS NOT SUPPORTED"));
      setTimeout(
        () => dispatch(onErrorFinish("IMAGE URL IS NOT SUPPORTED")),
        6000
      );
      return;
    }

    if (!finalImagepath.startsWith("https://")) {
      finalImagepath = "https://" + finalImagepath;
      setImagepath(finalImagepath);
    }

    setImageError([]);
    setImageApplied(true);
  };

  const submitInsert = async () => {
    if (!imageApplied) {
      dispatch(onError("Please apply the image first!"));
      setTimeout(
        () => dispatch(onErrorFinish("Please apply the image first!")),
        6000
      );
      return;
    }

    if (imageError.length > 0 || !sequence || sequence.length === 0) {
      dispatch(onError("INPUT DATA FORMAT ERROR"));
      setTimeout(
        () => dispatch(onErrorFinish("INPUT DATA FORMAT ERROR")),
        6000
      );
      return;
    }

    const response = await fetch("/api/category-item/insert", {
      method: "POST",
      headers: {
        Authorization: getAuthToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        description,
        imagepath,
        sequence,
        category_name: router.query.category,
      }),
    });

    const res = await response.json();

    if (res.success) {
      dispatch(onSuccess(res.message));
      setTimeout(() => dispatch(onSuccessFinish(res.message)), 6000);
      router.push(`/category/${router.query.category}`);
    } else {
      dispatch(onError(res.message));
      setTimeout(() => dispatch(onErrorFinish(res.message)), 6000);
    }
  };

  return (
    <div>
      <Head>
        <title>Insert New Category Item</title>
        <meta name="description" content="Insert New Category Item" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <form>
          <Grid item container justifyContent="center">
            <Grid
              item
              container
              alignItems="center"
              justifyContent="center"
              direction="column"
            >
              <Grid item container justifyContent="center">
                <Typography
                  variant="h4"
                  color="initial"
                  className="font-bold break-all mx-4"
                >
                  Insert New Item
                </Typography>
              </Grid>
              <Grid item className="my-5">
                <TextField
                  id=""
                  label="name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  variant="outlined"
                />
              </Grid>
              <Grid item className="my-5">
                <TextField
                  id=""
                  label="description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  variant="outlined"
                />
              </Grid>
              <Grid item className="my-5">
                <TextField
                  id=""
                  label="sequence"
                  value={sequence}
                  onChange={(e) => setSequence(e.target.value)}
                  variant="outlined"
                  type="number"
                  required
                />
              </Grid>
              <Grid item container className="my-5">
                <Grid
                  item
                  container
                  xs={12}
                  justifyContent="center"
                  className="mb-2"
                >
                  <TextField
                    id=""
                    label="imagepath"
                    value={imagepath}
                    onChange={(e) => {
                      setImageApplied(false);
                      setImagepath(e.target.value);
                    }}
                    variant="outlined"
                    required
                  />
                </Grid>
                <Grid item container xs={12} justifyContent="center">
                  <Button
                    variant="contained"
                    onClick={() => checkImagepath(imagepath)}
                    className="text-black bg-white"
                  >
                    Apply Image
                  </Button>
                </Grid>
              </Grid>

              <Button variant="text" onClick={() => submitInsert()}>
                Insert
              </Button>
              {imageApplied && imagepath !== "" && imageError.length === 0 ? (
                <Grid item container justifyContent="center">
                  <Grid xs={10}>
                    <Image
                      layout="responsive"
                      width="90vw"
                      height="100vh"
                      alt={imageAlt}
                      src={imagepath}
                    />
                  </Grid>
                </Grid>
              ) : (
                <></>
              )}
              <Button
                className="bg-black mb-5 text-white"
                variant="text"
                onClick={() => submitInsert()}
              >
                Insert
              </Button>
            </Grid>
          </Grid>
        </form>
      </main>
    </div>
  );
};

export default Category;
