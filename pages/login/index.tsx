import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { Grid, Typography, TextField, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { onLoadingFinish, onLoading } from "Redux/slice/loadingSlice";
import { onError, onErrorFinish } from "Redux/slice/errorSlice";
import { onSuccess, onSuccessFinish } from "Redux/slice/successSlice";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

const Category: NextPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const router = useRouter();
  // const isLoggedIn = loginCheck();

  // useEffect(() => {
  //   if (isLoggedIn) {
  //     router.push("/");
  //   }
  // }, [isLoggedIn]);
  const login = async (email: string, password: string) => {
    dispatch(onLoading("login"));
    let response = await fetch("/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    });
    dispatch(onLoadingFinish("login"));
    let result = await response.json();

    if (!result.success) {
      dispatch(onError(result.message));
      setTimeout(() => dispatch(onErrorFinish(result.message)), 6000);
      return;
    }

    if (result.token) {
      dispatch(onSuccess(result.message));
      setTimeout(() => dispatch(onSuccessFinish(result.message)), 6000);
      const localAuth: any = localStorage.getItem("auth") || null;
      const auth = localAuth ? JSON.parse(localAuth) : {};
      localStorage.setItem(
        "auth",
        JSON.stringify({
          ...auth,
          token: result.token,
          role: result.role,
        })
      );

      router.push("/");
    } else {
      dispatch(onError("unexpected error occured!"));
      setTimeout(
        () => dispatch(onErrorFinish("unexpected error occured!")),
        6000
      );
    }
  };

  return (
    <div>
      <Head>
        <title>Login</title>
        <meta name="description" content="Login" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <form>
          <Grid item container justifyContent="center" className="h-screen">
            <Grid
              item
              container
              alignItems="center"
              justifyContent="center"
              direction="column"
            >
              <Grid item>
                <Typography variant="h1" color="initial">
                  Login
                </Typography>
              </Grid>
              <Grid item className="m-5">
                <TextField
                  id=""
                  label="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  variant="outlined"
                />
              </Grid>
              <Grid item className="m-5">
                <TextField
                  id=""
                  label="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  variant="outlined"
                  type="password"
                />
              </Grid>
              <Button variant="text" onClick={() => login(email, password)}>
                Login
              </Button>
            </Grid>
          </Grid>
        </form>
      </main>
    </div>
  );
};

export default Category;
