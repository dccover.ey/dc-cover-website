import type { NextPage } from "next";
import Link from "next/link";
import Image from "next/image";
import { Grid, Typography } from "@mui/material";
import Head from "next/head";
import CancelIcon from "@mui/icons-material/Cancel";
import { useState } from "react";

const Summary: NextPage = () => {
  const [openQuickNav, setOpenQuickNav] = useState(false);

  return (
    <div>
      <Head>
        <title>Summary</title>
        <meta name="description" content="Summary" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Grid container direction="column">
          <Grid item container>
            <Grid item xs={12} id="opening_1">
              <Image
                src="/item-assets/summary/opening1.png"
                height={135}
                width="100%"
                alt="opening1"
                layout="responsive"
              />
            </Grid>
            <Grid item container id="opening_2">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/opening2.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="about_us">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/about-us.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="mission">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/mission.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="vision">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/vision.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="solution">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/solution.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="product">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/product.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="team">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/team.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="art_process">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/art-process.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container id="artwork">
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/artwork-1.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
            <Grid item container>
              <Grid item xs={12}>
                <Image
                  src="/item-assets/summary/artwork-2.png"
                  height={135}
                  width="100%"
                  alt="opening1"
                  layout="responsive"
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          container
          onClick={() => setOpenQuickNav((state) => !state)}
          className={`${
            openQuickNav ? "quickNav" : ""
          } p-2 text-white right-0 fixed z-100 top-5 bg-gray-500 w-fit rounded-tl-xl rounded-bl-xl cursor-pointer`}
        >
          {!openQuickNav && <Typography>Quick Nav</Typography>}
          {openQuickNav && (
            <ul>
              <li>
                <CancelIcon fontSize="large" />
              </li>
              <li className="m-2">
                <Link href="#opening_1">Opening 1</Link>
              </li>
              <li className="m-2">
                <Link href="#opening_2">Opening 2</Link>
              </li>
              <li className="m-2">
                <Link href="#about_us">About Us</Link>
              </li>
              <li className="m-2">
                <Link href="#mission">Mission</Link>
              </li>
              <li className="m-2">
                <Link href="#vision">Vision</Link>
              </li>
              <li className="m-2">
                <Link href="#solution">Solution</Link>
              </li>
              <li className="m-2">
                <Link href="#product">Product</Link>
              </li>
              <li className="m-2">
                <Link href="#team">Team</Link>
              </li>
              <li className="m-2">
                <Link href="#art_process">Art Process</Link>
              </li>
              <li className="m-2">
                <Link href="#artwork">Artwork</Link>
              </li>
              <li className="m-2">
                <Link href="/" passHref>
                  <Typography className="text-red-400 font-bold">
                    Back to home
                  </Typography>
                </Link>
              </li>
            </ul>
          )}
        </Grid>
      </main>
    </div>
  );
};

export default Summary;
