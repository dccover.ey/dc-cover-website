const jwt = require("jsonwebtoken");

export async function checkToken(token: string | undefined) {
  if (!token) {
    return false;
  }

  const formattedToken = token.replace(/^Bearer\s+/, "");

  try {
    const verified = jwt.verify(formattedToken, process.env.JWT_AUTH_SECRET);

    if (verified) {
      return true;
    }
  } catch (e) {
    return false;
  }

  return false;
}
