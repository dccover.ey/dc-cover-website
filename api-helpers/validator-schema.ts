import Joi from "joi";

export const insertCategorySchema = Joi.object({
  name: Joi.string().required(),
  imagepath: Joi.string().required(),
  sequence: Joi.number().required(),
  description: Joi.string(),
});

export const deleteCategorySchema = Joi.object({
  category_id: Joi.string().required(),
});

export const insertCategoryItemSchema = Joi.object({
  name: Joi.string().required(),
  imagepath: Joi.string().required(),
  sequence: Joi.number().required(),
  category_name: Joi.string().required(),
  description: Joi.string(),
});

export const deleteCategoryItemSchema = Joi.object({
  category_item_id: Joi.string().required(),
});

export const editCategorySchema = Joi.object({
  name: Joi.string().required(),
  old_name: Joi.string().required(),
  imagepath: Joi.string().required(),
  sequence: Joi.number().required(),
  description: Joi.string(),
});

export const editCategoryItemSchema = Joi.object({
  name: Joi.string().required(),
  old_name: Joi.string().required(),
  imagepath: Joi.string().required(),
  sequence: Joi.number().required(),
  description: Joi.string(),
  category_id: Joi.string().required(),
});

export const retrieveCategorySchema = Joi.object({
  id: Joi.string(),
  name: Joi.string(),
  per_page: Joi.number(),
});

export const retrieveCategoryItemSchema = Joi.object({
  id: Joi.string(),
  name: Joi.string(),
  per_page: Joi.number(),
  category_id: Joi.string(),
  category_name: Joi.string(),
});
