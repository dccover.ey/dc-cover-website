import mongoose from "mongoose";

const UserSchema = new mongoose.Schema(
  {
    email: String,
    password: String,
    role: String,
  },
  { timestamps: { createdAt: "created_at" } }
);

export default mongoose.models.User || mongoose.model("User", UserSchema);
