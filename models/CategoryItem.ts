import mongoose from "mongoose";

const Schema = mongoose.Schema;

const categoryItemSchema = new Schema(
  {
    description: { type: String, required: false },
    name: { type: String, required: true, unique: true },
    imagepath: { type: String, required: true },
    sequence: { type: Number, required: true },
    category_id: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Category",
    },
  },
  { timestamps: { createdAt: "created_at" } }
);

export default mongoose.models.CategoryItem ||
  mongoose.model("CategoryItem", categoryItemSchema);
