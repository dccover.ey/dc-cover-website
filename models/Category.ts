import mongoose from "mongoose";

const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    description: { type: String, required: false },
    name: { type: String, required: true, unique: true },
    imagepath: { type: String, required: true },
    sequence: { type: Number, required: true },
    category_items: [
      { type: mongoose.Types.ObjectId, required: false, ref: "CategoryItem" },
    ],
  },
  { timestamps: { createdAt: "created_at" } }
);

export default mongoose.models.Category ||
  mongoose.model("Category", categorySchema);
