import { createSlice } from "@reduxjs/toolkit";

export const LoadingSlice = createSlice({
  name: "loading",
  initialState: {
    isLoading: false,
    action: {},
  },
  reducers: {
    onLoading: (state, action) => {
      state.isLoading = true;
      state.action = { ...state.action, [action.payload]: true };
    },
    onLoadingFinish: (state, action) => {
      state.isLoading = false;
      delete state.action[action.payload];
      Object.keys(state.action).length <= 0
        ? (state.isLoading = false)
        : (state.isLoading = true);
    },
  },
});

// Action creators are generated for each case reducer function
export const { onLoading, onLoadingFinish } = LoadingSlice.actions;

export default LoadingSlice.reducer;
