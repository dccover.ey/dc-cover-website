import { configureStore } from "@reduxjs/toolkit";
import loadingSlice from "../slice/loadingSlice";
import errorSlice from "../slice/errorSlice";
import successSlice from "../slice/successSlice";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  loading: loadingSlice,
  error: errorSlice,
  success: successSlice,
});

export default configureStore({
  reducer: rootReducer,
});
