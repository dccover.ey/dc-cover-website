export function readAllFileFromFolder() {
  const remote = window.require("electron").remote;
  const electronFs = remote.require("fs");
  const electronDialog = remote.dialog;

  // return require("/public");
}

function getImagePaths(directory: any) {
  let images: any = [];
  directory
    .keys()
    .map((item: string, index: number) => images.push(item.replace("./", "")));
  return images;
}
