import { useRouter } from "next/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { onError, onErrorFinish } from "Redux/slice/errorSlice";
import { onLoading, onLoadingFinish } from "Redux/slice/loadingSlice";

export const loginCheck = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    const checkAuth = async (token: string) => {
      if (token && token.length > 0) {
        dispatch(onLoading("check auth"));
        const response = await fetch("/api/check-auth", {
          method: "GET",
          credentials: "include",
          headers: {
            Authorization: token,
            "Content-Type": "application/json",
          },
        });
        dispatch(onLoadingFinish("check auth"));

        let result = await response.json();

        if (!result.success) {
          dispatch(onError(result.message));
          setTimeout(() => dispatch(onErrorFinish(result.message)), 6000);
          localStorage.removeItem("auth");
          return false;
        }

        if (router.pathname === "/login") {
          router.push("/");
        }
        return true;
      }

      return false;
    };

    const token = getAuthToken();

    if (token) {
      checkAuth(token);
    }
  }, []);

  return;
};

export function isLoggedIn() {
  const token = getAuthToken();

  if (token) {
    return true;
  } else {
    return false;
  }
}

export function getAuthToken() {
  if (typeof window === "undefined") {
    return null;
  }
  const localAuth = localStorage.getItem("auth") || null;
  const auth = localAuth ? JSON.parse(localAuth) : {};

  return auth.token;
}
