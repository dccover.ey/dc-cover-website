import { Grid, Typography } from "@mui/material";
import Link from "next/link";
import Image from "next/image";
import { useBreakPoints } from "Helpers/breakpoint-helper";

export default function CustomFooter() {
  const mdUp = useBreakPoints("mdUp");
  return (
    <footer>
      <Grid
        item
        container
        direction="column"
        style={{ backgroundColor: "#212121" }}
        className="text-white py-4 px-4  text-justify"
      >
        <Grid item className="mb-4 border-l-2 pl-1">
          <Typography variant="h6" className="font-bold">
            PT KAIROS SINERGI INDONESIA
          </Typography>
        </Grid>
        <Grid container>
          <Grid item container xs={12} md={4}>
            <Grid item container direction="column">
              <Typography variant="body2" className="font-bold text-[20px]">
                Office :
              </Typography>
              <Typography variant="body2">
                Menara Rajawali, 16th Floor. Jl. DR Ide Anak Agung Gde Agung,
                Mega Kuningan, Jakarta Selatan
              </Typography>
              <Grid item>
                <Link href="/">www.dc-cover.com</Link>
              </Grid>
            </Grid>
            <Grid item container className="my-2">
              <hr className="w-full" />
            </Grid>
            <Grid item container direction="column">
              <Typography variant="body2">
                <span className="font-bold text-[20px]">Workshop : </span>
              </Typography>
              <Typography variant="body2">
                Komplek Sawo Griya Kencana I Blok UH 2 Limo, Jl. Arthasaya
                Depok, Jawa Barat, Indonesia
              </Typography>
              <Typography variant="body2">Mobile +62817801718</Typography>
            </Grid>
          </Grid>

          {!mdUp ? (
            <Grid item container className="my-2">
              <hr className="w-full" />
            </Grid>
          ) : (
            <></>
          )}
          <Grid item container justifyContent="center" md={4}>
            <Grid item xs={6}>
              <Image
                layout="responsive"
                src="/item-assets/summary/barcode-assets/new-barcode-final.png"
                alt="barcode"
                height="120px"
                width="100%"
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </footer>
  );
}
