import {
  AppBar,
  Toolbar,
  Typography,
  Grid,
  IconButton,
  Drawer,
  Button,
} from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { useBreakPoints } from "Helpers/breakpoint-helper";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import MenuIcon from "@mui/icons-material/Menu";
import { isLoggedIn } from "Helpers/auth-helper";

export default function CustomAppBar() {
  const [openDrawer, setOpenDrawer] = useState(false);
  const smDown = useBreakPoints("smDown");
  const router = useRouter();

  const loginRef = useRef(false);

  const isIn = isLoggedIn();
  useEffect(() => {
    loginRef.current = isLoggedIn();
  }, [isIn]);

  const logout = () => {
    localStorage.removeItem("auth");
    setOpenDrawer(false);
    router.reload();
  };

  return router.pathname !== "/summary" ? (
    <>
      <AppBar position="fixed" className="bg-black sticky">
        <Toolbar>
          <Grid container alignItems="center">
            <Grid item container xs={7} md={2} alignItems="center">
              {smDown && (
                <Grid item>
                  <IconButton
                    onClick={() => {
                      router.back();
                    }}
                  >
                    <ArrowBackIcon className="text-white" fontSize="large" />
                  </IconButton>
                </Grid>
              )}
              <Grid item className="cursor-pointer">
                <Link href="/" passHref>
                  <div>
                    <Image
                      src={"/favicon.ico" || "/"}
                      alt="DC-Cover Icon"
                      width={75}
                      height={75}
                    />
                  </div>
                </Link>
              </Grid>
            </Grid>
            <Grid item container xs={5} md={10} justifyContent="flex-end">
              {!smDown && (
                <Grid
                  item
                  container
                  direction="row"
                  alignItems="center"
                  sm={10}
                  justifyContent="space-around"
                >
                  {/* {menuItems()} */}
                </Grid>
              )}
              <Grid item>
                <IconButton onClick={() => setOpenDrawer(true)}>
                  <MenuIcon className="text-white" fontSize="large" />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Drawer
        anchor="right"
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
      >
        <Grid
          container
          direction="column"
          style={{ background: "#212121" }}
          className="text-green-500 h-full w-52 p-6"
          alignItems="flex-end"
        >
          {loginRef.current ? (
            <>
              <Typography variant="h6">Logged in as admin...</Typography>
              <Button
                variant="text"
                className="text-red-500"
                onClick={() => logout()}
              >
                Logout
              </Button>
              <Grid item className="border-2 border-white my-3 w-full" />
            </>
          ) : (
            <></>
          )}
          <Link href="/summary" passHref>
            <div onClick={() => setOpenDrawer(false)}>
              <Typography
                variant="h5"
                color="initial"
                className="font-bold break-all text-white hover:text-green-500 cursor-pointer"
              >
                Summary
              </Typography>
            </div>
          </Link>
        </Grid>
      </Drawer>
    </>
  ) : (
    <></>
  );
}
