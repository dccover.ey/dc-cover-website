import { useSelector } from "react-redux";

export default function Spinner() {
  //   const classes = useStyles();
  const loading = useSelector((state: any) => state.loading);

  return (
    loading.isLoading && (
      <div className="topDownAnim">
        <div className="lds-dual-ring" />
      </div>
    )
  );
}
