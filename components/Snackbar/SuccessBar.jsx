import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { useDispatch, useSelector } from "react-redux";
import { clearSuccess } from "Redux/slice/successSlice";

export default function SuccessBar() {
  const dispatch = useDispatch();
  const success = useSelector((state) => state.success);

  const handleClose = () => {
    dispatch(clearSuccess());
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      open={success.isSuccess}
      autoHideDuration={6000}
      onClose={handleClose}
      message={
        <div
          style={{
            color: "#2ad20f",
          }}
        >
          {Object.keys(success.message).map((msg, idx) => (
            <div key={`${msg}-${idx}`}>{msg}</div>
          ))}
        </div>
      }
      action={
        <>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </>
      }
    />
  );
}
