import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { useDispatch, useSelector } from "react-redux";
import { clearError } from "Redux/slice/errorSlice";

export default function ErrorBar() {
  const dispatch = useDispatch();
  const error = useSelector((state) => state.error);

  const handleClose = () => {
    dispatch(clearError());
  };

  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      open={error.isError}
      autoHideDuration={6000}
      onClose={handleClose}
      message={
        <div style={{ color: "#df0000" }}>
          {Object.keys(error.message).map((msg, idx) => (
            <div key={`${msg}-${idx}`}>{msg}</div>
          ))}
        </div>
      }
      action={
        <>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </>
      }
    />
  );
}
