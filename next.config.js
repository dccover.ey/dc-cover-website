/** @type {import('next').NextConfig} */
const { createSecureHeaders } = require("next-secure-headers");

const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["i.imgur.com", "imgur.com"],
  },

  async headers() {
    return [{ source: "/(.*)", headers: createSecureHeaders() }];
  },
};

module.exports = nextConfig;
