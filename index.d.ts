interface connProm {
  conn: Promise;
  promise: Promise;
}

declare global {
  function someFunction(): string;
  var mongoose: connProm;
}

export {};
